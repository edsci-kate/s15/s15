console.log('Hello, World')
// Details
const details = {
    fName: "Ashley",
    lName: "Kate",
    age: 17,
    hobbies : [
        "listening music", "watching k-drama", "sleeping"
    ] ,
    workAddress: {
        housenumber: "Blk 26",
        street: "Lawaan St.",
        city: "Caloocan ",
        state: "Philippines",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");